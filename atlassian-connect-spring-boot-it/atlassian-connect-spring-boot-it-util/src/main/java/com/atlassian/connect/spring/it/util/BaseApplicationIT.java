package com.atlassian.connect.spring.it.util;

import com.atlassian.connect.spring.AtlassianHost;
import com.atlassian.connect.spring.AtlassianHostRepository;
import com.atlassian.connect.spring.AtlassianHostUser;
import com.atlassian.connect.spring.AtlassianHostUser.AtlassianHostUserBuilder;
import org.junit.After;
import org.junit.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.env.ConfigurableEnvironment;
import org.springframework.security.authentication.TestingAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import java.util.Optional;

public class BaseApplicationIT {

    protected MockMvc mvc;

    @Autowired
    protected WebApplicationContext wac;

    @Autowired
    protected AtlassianHostRepository hostRepository;

    @Autowired
    private ConfigurableEnvironment environment;

    @Before
    public void setup() {
        this.mvc = MockMvcBuilders.webAppContextSetup(this.wac).apply(SecurityMockMvcConfigurers.springSecurity()).build();
    }

    @After
    public void tearDown() {
        SecurityContextHolder.clearContext();
        hostRepository.deleteAll();
    }

    protected String getServerAddress() {
        String port = environment.getProperty("local.server.port", "8080");
        return "http://localhost:" + port;
    }

    protected void setJwtAuthenticatedPrincipal(AtlassianHost host) {
        setJwtAuthenticatedPrincipal(host, null);
    }

    protected void setJwtAuthenticatedPrincipal(AtlassianHost host, String userAccountId) {
        AtlassianHostUserBuilder hostUserBuilder = AtlassianHostUser.builder(host);
        if(userAccountId != null) {
            hostUserBuilder.withUserAccountId(userAccountId);
        }
        TestingAuthenticationToken authentication = new TestingAuthenticationToken(hostUserBuilder.build(), null);
        authentication.setAuthenticated(true);
        SecurityContextHolder.getContext().setAuthentication(authentication);
    }
}
